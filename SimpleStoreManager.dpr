program SimpleStoreManager;

uses
  Vcl.Forms,
  UMainMenu in 'Units\UMainMenu.pas' {FMainMenu},
  ULClientsList in 'Units\ULClientsList.pas' {FLClientsList},
  ULOrdersList in 'Units\ULOrdersList.pas' {FLOrdersList},
  ULProductsList in 'Units\ULProductsList.pas' {FLProductsList},
  UClientsForm in 'Units\UClientsForm.pas' {FClientsForm},
  UProductsForm in 'Units\UProductsForm.pas' {FProductsForm},
  UOrdersForm in 'Units\UOrdersForm.pas' {FOrdersForm};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFMainMenu, FMainMenu);
  Application.CreateForm(TFLClientsList, FLClientsList);
  Application.CreateForm(TFLOrdersList, FLOrdersList);
  Application.CreateForm(TFLProductsList, FLProductsList);
  Application.CreateForm(TFClientsForm, FClientsForm);
  Application.CreateForm(TFProductsForm, FProductsForm);
  Application.CreateForm(TFOrdersForm, FOrdersForm);
  Application.Run;
end.
