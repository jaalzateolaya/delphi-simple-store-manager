unit ULClientsList;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf,
  FireDAC.DApt, FireDAC.VCLUI.Wait, FireDAC.Comp.UI, FireDAC.Phys.IBBase,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, Vcl.ExtCtrls, Vcl.Grids,
  Vcl.DBGrids, Vcl.Buttons;

type
  TFLClientsList = class(TForm)
    FDConnection1: TFDConnection;
    FDQuery1: TFDQuery;
    FDPhysFBDriverLink1: TFDPhysFBDriverLink;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    DataSource1: TDataSource;
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    FDQuery1ID: TIntegerField;
    FDQuery1NAME: TStringField;
    FDQuery1LASTNAME: TStringField;
    FDQuery1ADDRESS: TStringField;
    FDUpdateSQL1: TFDUpdateSQL;
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FLClientsList: TFLClientsList;

implementation

{$R *.dfm}

uses UClientsForm;

procedure TFLClientsList.SpeedButton1Click(Sender: TObject);
begin
  FDQuery1.Delete;
end;

procedure TFLClientsList.SpeedButton2Click(Sender: TObject);
begin
  FDQuery1.Edit;
  FClientsForm.ShowModal;
end;

procedure TFLClientsList.SpeedButton3Click(Sender: TObject);
begin
  FDQuery1.Append;
  FClientsForm.ShowModal;
end;

end.
