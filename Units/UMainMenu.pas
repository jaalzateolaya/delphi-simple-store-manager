unit UMainMenu;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Menus;

type
  TFMainMenu = class(TForm)
    MainMenu1: TMainMenu;
    Ventas1: TMenuItem;
    Clientes1: TMenuItem;
    Pedidos1: TMenuItem;
    Almacen1: TMenuItem;
    Productos1: TMenuItem;
    procedure onMenuClientsClick(Sender: TObject);
    procedure onMenuOrdersClick(Sender: TObject);
    procedure onMenuProductsClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FMainMenu: TFMainMenu;

implementation

{$R *.dfm}

uses ULClientsList, ULOrdersList, ULProductsList;

procedure TFMainMenu.onMenuClientsClick(Sender: TObject);
begin
  FLClientsList.ShowModal;
end;

procedure TFMainMenu.onMenuOrdersClick(Sender: TObject);
begin
  FLOrdersList.ShowModal;
end;

procedure TFMainMenu.onMenuProductsClick(Sender: TObject);
begin
  FLProductsList.ShowModal;
end;

end.
