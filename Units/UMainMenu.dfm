object FMainMenu: TFMainMenu
  Left = 0
  Top = 0
  Caption = 'Simple Store Manager'
  ClientHeight = 511
  ClientWidth = 922
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  Menu = MainMenu1
  TextHeight = 15
  object MainMenu1: TMainMenu
    Left = 40
    Top = 416
    object Ventas1: TMenuItem
      Caption = 'Ventas'
      object Clientes1: TMenuItem
        Caption = 'Clientes'
        OnClick = onMenuClientsClick
      end
      object Pedidos1: TMenuItem
        Caption = 'Pedidos'
        OnClick = onMenuOrdersClick
      end
    end
    object Almacen1: TMenuItem
      Caption = 'Almacen'
      object Productos1: TMenuItem
        Caption = 'Productos'
        OnClick = onMenuProductsClick
      end
    end
  end
end
