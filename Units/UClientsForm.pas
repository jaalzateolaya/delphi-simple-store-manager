unit UClientsForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Buttons, Data.DB, Vcl.StdCtrls,
  Vcl.Mask, Vcl.ExtCtrls, Vcl.DBCtrls;

type
  TFClientsForm = class(TForm)
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FClientsForm: TFClientsForm;

implementation

{$R *.dfm}

uses ULClientsList;

procedure TFClientsForm.SpeedButton1Click(Sender: TObject);
begin
  FLClientsList.FDQuery1.Post;
  Close;
end;

procedure TFClientsForm.SpeedButton2Click(Sender: TObject);
begin
  FLClientsList.FDQuery1.Cancel;
  Close;
end;

end.
