object FLOrdersList: TFLOrdersList
  Left = 0
  Top = 0
  Caption = 'Pedidos'
  ClientHeight = 442
  ClientWidth = 628
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  TextHeight = 15
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 628
    Height = 393
    Align = alTop
    DataSource = DataSource1
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'ID'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CREATED_AT'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CONCATENATION'
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 407
    Width = 628
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitLeft = 200
    ExplicitTop = 423
    object SpeedButton1: TSpeedButton
      AlignWithMargins = True
      Left = 512
      Top = 3
      Width = 113
      Height = 29
      Align = alRight
      Caption = 'Borrar'
      OnClick = SpeedButton1Click
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitHeight = 50
    end
    object SpeedButton2: TSpeedButton
      AlignWithMargins = True
      Left = 393
      Top = 3
      Width = 113
      Height = 29
      Align = alRight
      Caption = 'Editar'
      OnClick = SpeedButton2Click
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitHeight = 50
    end
    object SpeedButton3: TSpeedButton
      AlignWithMargins = True
      Left = 274
      Top = 3
      Width = 113
      Height = 29
      Align = alRight
      Caption = 'Crear'
      OnClick = SpeedButton3Click
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitHeight = 50
    end
  end
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Database=C:\Data\MY_STORE.FDB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'DriverID=FB')
    Connected = True
    LoginPrompt = False
    Left = 272
    Top = 104
  end
  object FDOrdersQuery1: TFDQuery
    Active = True
    Connection = FDConnection1
    UpdateOptions.AssignedValues = [uvCountUpdatedRecords]
    UpdateOptions.CountUpdatedRecords = False
    UpdateObject = FDUpdateSQL1
    SQL.Strings = (
      
        'SELECT o.id, o.created_at, o.client_id, c.lastname || '#39', '#39' || c.' +
        'name FROM orders o JOIN clients c ON o.client_id = c.id;')
    Left = 272
    Top = 160
    object FDOrdersQuery1ID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object FDOrdersQuery1CREATED_AT: TSQLTimeStampField
      DisplayLabel = 'Fecha'
      FieldName = 'CREATED_AT'
      Origin = 'CREATED_AT'
      Required = True
    end
    object FDOrdersQuery1CLIENT_ID: TIntegerField
      FieldName = 'CLIENT_ID'
      Origin = 'CLIENT_ID'
      Required = True
    end
    object FDOrdersQuery1CONCATENATION2: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Cliente'
      FieldName = 'CONCATENATION'
      Origin = 'CONCATENATION'
      ProviderFlags = []
      ReadOnly = True
      Size = 202
    end
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 272
    Top = 216
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 272
    Top = 272
  end
  object DataSource1: TDataSource
    DataSet = FDOrdersQuery1
    Left = 328
    Top = 160
  end
  object FDUpdateSQL1: TFDUpdateSQL
    Connection = FDConnection1
    InsertSQL.Strings = (
      'INSERT INTO ORDERS'
      '(CLIENT_ID, CREATED_AT)'
      'VALUES (:NEW_CLIENT_ID, :NEW_CREATED_AT)'
      'RETURNING ID, CLIENT_ID, CREATED_AT')
    ModifySQL.Strings = (
      'UPDATE ORDERS'
      'SET CLIENT_ID = :NEW_CLIENT_ID, CREATED_AT = :NEW_CREATED_AT'
      'WHERE ID = :OLD_ID'
      'RETURNING ID, CLIENT_ID, CREATED_AT')
    DeleteSQL.Strings = (
      'DELETE FROM ORDERS'
      'WHERE ID = :OLD_ID')
    FetchRowSQL.Strings = (
      'SELECT ID, CLIENT_ID, CREATED_AT'
      'FROM ORDERS'
      'WHERE ID = :OLD_ID')
    Left = 200
    Top = 160
  end
end
