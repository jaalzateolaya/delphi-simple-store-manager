unit ULProductsList;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Param, FireDAC.Stan.Error, FireDAC.DatS,
  FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt,
  FireDAC.UI.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Phys,
  FireDAC.Phys.FB, FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, FireDAC.Comp.UI,
  FireDAC.Phys.IBBase, FireDAC.Comp.Client, FireDAC.Comp.DataSet, Vcl.Buttons,
  Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids;

type
  TFLProductsList = class(TForm)
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    FDProductsQuery1: TFDQuery;
    ProductsDataSource1: TDataSource;
    FDConnection1: TFDConnection;
    FDUpdateSQL1: TFDUpdateSQL;
    FDPhysFBDriverLink1: TFDPhysFBDriverLink;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    FDProductsQuery1ID: TIntegerField;
    FDProductsQuery1NAME: TStringField;
    FDProductsQuery1PRICE: TFloatField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FLProductsList: TFLProductsList;

implementation

{$R *.dfm}

uses UProductsForm;

procedure TFLProductsList.SpeedButton1Click(Sender: TObject);
begin
  FDProductsQuery1.Append;
  FProductsForm.ShowModal;
end;

procedure TFLProductsList.SpeedButton2Click(Sender: TObject);
begin
  FDProductsQuery1.Delete;
end;

procedure TFLProductsList.SpeedButton3Click(Sender: TObject);
begin
  FDProductsQuery1.Edit;
  FProductsForm.ShowModal;
end;

end.
