object FProductsForm: TFProductsForm
  Left = 0
  Top = 0
  Caption = 'Formulario de Producto'
  ClientHeight = 184
  ClientWidth = 317
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  TextHeight = 15
  object Label1: TLabel
    Left = 8
    Top = 32
    Width = 44
    Height = 15
    Caption = 'Nombre'
    FocusControl = DBEdit1
  end
  object Label2: TLabel
    Left = 8
    Top = 80
    Width = 33
    Height = 15
    Caption = 'Precio'
    FocusControl = DBEdit2
  end
  object SpeedButton1: TSpeedButton
    Left = 8
    Top = 152
    Width = 121
    Height = 22
    Caption = '&Guardar'
    OnClick = SpeedButton1Click
  end
  object SpeedButton2: TSpeedButton
    Left = 187
    Top = 152
    Width = 121
    Height = 22
    Caption = '&Cancelar'
    OnClick = SpeedButton2Click
  end
  object DBEdit1: TDBEdit
    Left = 8
    Top = 48
    Width = 300
    Height = 23
    DataField = 'NAME'
    DataSource = DataSource1
    TabOrder = 0
  end
  object DBEdit2: TDBEdit
    Left = 8
    Top = 96
    Width = 300
    Height = 23
    DataField = 'PRICE'
    DataSource = DataSource1
    TabOrder = 1
  end
  object DataSource1: TDataSource
    DataSet = FLProductsList.FDProductsQuery1
    Left = 280
    Top = 8
  end
end
