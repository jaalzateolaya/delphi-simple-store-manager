unit ULOrdersList;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Phys.FB, FireDAC.Phys.FBDef, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.VCLUI.Wait, FireDAC.Comp.Client,
  FireDAC.Comp.UI, FireDAC.Phys.IBBase, FireDAC.Comp.DataSet, Vcl.Buttons,
  Vcl.ExtCtrls, Vcl.Grids, Vcl.DBGrids;

type
  TFLOrdersList = class(TForm)
    DBGrid1: TDBGrid;
    Panel1: TPanel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    FDConnection1: TFDConnection;
    FDOrdersQuery1: TFDQuery;
    FDOrdersQuery1ID: TIntegerField;
    FDPhysFBDriverLink1: TFDPhysFBDriverLink;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    DataSource1: TDataSource;
    FDUpdateSQL1: TFDUpdateSQL;
    FDOrdersQuery1CREATED_AT: TSQLTimeStampField;
    FDOrdersQuery1CLIENT_ID: TIntegerField;
    FDOrdersQuery1CONCATENATION2: TStringField;
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FLOrdersList: TFLOrdersList;

implementation

{$R *.dfm}

uses UOrdersForm;

procedure TFLOrdersList.SpeedButton1Click(Sender: TObject);
begin
  FDOrdersQuery1.Delete;
end;

procedure TFLOrdersList.SpeedButton2Click(Sender: TObject);
begin
  FDOrdersQuery1.Edit;
  FOrdersForm.ShowModal;
end;

procedure TFLOrdersList.SpeedButton3Click(Sender: TObject);
begin
  FDOrdersQuery1.Append;
  FOrdersForm.ShowModal;
end;

end.
