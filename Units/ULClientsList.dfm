object FLClientsList: TFLClientsList
  Left = 0
  Top = 0
  Caption = 'Clientes'
  ClientHeight = 462
  ClientWidth = 628
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  TextHeight = 15
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 628
    Height = 416
    Align = alTop
    DataSource = DataSource1
    ReadOnly = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'ID'
        Width = 50
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NAME'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LASTNAME'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ADDRESS'
        Width = 200
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 427
    Width = 628
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitTop = 426
    ExplicitWidth = 624
    object SpeedButton1: TSpeedButton
      AlignWithMargins = True
      Left = 512
      Top = 3
      Width = 113
      Height = 29
      Align = alRight
      Caption = 'Borrar'
      OnClick = SpeedButton1Click
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitHeight = 50
    end
    object SpeedButton2: TSpeedButton
      AlignWithMargins = True
      Left = 393
      Top = 3
      Width = 113
      Height = 29
      Align = alRight
      Caption = 'Editar'
      OnClick = SpeedButton2Click
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitHeight = 50
    end
    object SpeedButton3: TSpeedButton
      AlignWithMargins = True
      Left = 274
      Top = 3
      Width = 113
      Height = 29
      Align = alRight
      Caption = 'Crear'
      OnClick = SpeedButton3Click
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitHeight = 50
    end
  end
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Database=C:\Data\MY_STORE.FDB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'DriverID=FB')
    Connected = True
    LoginPrompt = False
    Left = 272
    Top = 104
  end
  object FDQuery1: TFDQuery
    Active = True
    Connection = FDConnection1
    UpdateOptions.AssignedValues = [uvCountUpdatedRecords]
    UpdateOptions.CountUpdatedRecords = False
    UpdateObject = FDUpdateSQL1
    SQL.Strings = (
      'SELECT * FROM CLIENTS;')
    Left = 272
    Top = 160
    object FDQuery1ID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object FDQuery1NAME: TStringField
      DisplayLabel = 'Nombre'
      FieldName = 'NAME'
      Origin = 'NAME'
      Required = True
      Size = 100
    end
    object FDQuery1LASTNAME: TStringField
      DisplayLabel = 'Apellido'
      FieldName = 'LASTNAME'
      Origin = '"LASTNAME"'
      Required = True
      Size = 100
    end
    object FDQuery1ADDRESS: TStringField
      DisplayLabel = 'Direcci'#243'n'
      FieldName = 'ADDRESS'
      Origin = 'ADDRESS'
      Size = 100
    end
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 272
    Top = 216
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 272
    Top = 272
  end
  object DataSource1: TDataSource
    DataSet = FDQuery1
    Left = 328
    Top = 160
  end
  object FDUpdateSQL1: TFDUpdateSQL
    Connection = FDConnection1
    InsertSQL.Strings = (
      'INSERT INTO CLIENTS'
      '(NAME, LASTNAME, ADDRESS)'
      'VALUES (:NEW_NAME, :NEW_LASTNAME, :NEW_ADDRESS)'
      'RETURNING ID, NAME, LASTNAME, ADDRESS')
    ModifySQL.Strings = (
      'UPDATE CLIENTS'
      
        'SET NAME = :NEW_NAME, LASTNAME = :NEW_LASTNAME, ADDRESS = :NEW_A' +
        'DDRESS'
      'WHERE ID = :OLD_ID'
      'RETURNING ID, NAME, LASTNAME, ADDRESS')
    DeleteSQL.Strings = (
      'DELETE FROM CLIENTS'
      'WHERE ID = :OLD_ID')
    FetchRowSQL.Strings = (
      'SELECT ID, NAME, LASTNAME, ADDRESS'
      'FROM CLIENTS'
      'WHERE ID = :OLD_ID')
    Left = 200
    Top = 160
  end
end
