object FOrdersForm: TFOrdersForm
  Left = 0
  Top = 0
  Caption = 'Formulario de Pedido'
  ClientHeight = 439
  ClientWidth = 628
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  OnClose = FormClose
  TextHeight = 15
  object SpeedButtonAddProduct: TSpeedButton
    Left = 464
    Top = 102
    Width = 97
    Height = 22
    Caption = '&A'#241'adir Producto'
    OnClick = SpeedButtonAddProductClick
  end
  object LabelClientAddress: TLabel
    Left = 384
    Top = 57
    Width = 78
    Height = 15
    Caption = 'Direcci'#243'n: N/A'
  end
  object LabelClientID: TLabel
    Left = 338
    Top = 57
    Width = 39
    Height = 15
    Caption = 'ID: N/A'
  end
  object LabelOrderId: TLabel
    Left = 8
    Top = 8
    Width = 54
    Height = 15
    Caption = 'ID Pedido:'
  end
  object DBGrid1: TDBGrid
    Left = 8
    Top = 142
    Width = 612
    Height = 250
    DataSource = DataSourceOrdersDetails
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'ORDER_LINE'
        Title.Caption = 'Linea'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRODUCT_NAME'
        Title.Caption = 'Producto'
        Width = 431
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ORDER_QUANTITY'
        Title.Caption = 'Cantidad'
        Width = 97
        Visible = True
      end>
  end
  object TLabeledEditClient: TLabeledEdit
    Left = 8
    Top = 54
    Width = 300
    Height = 23
    EditLabel.Width = 37
    EditLabel.Height = 15
    EditLabel.Caption = 'Cliente'
    TabOrder = 1
    Text = ''
    OnExit = TLabeledEditClientExit
    OnKeyPress = TLabeledEditClientKeyPress
  end
  object TLabeledEditQuantity: TLabeledEdit
    Left = 337
    Top = 102
    Width = 110
    Height = 23
    EditLabel.Width = 48
    EditLabel.Height = 15
    EditLabel.Caption = 'Cantidad'
    NumbersOnly = True
    TabOrder = 2
    Text = ''
  end
  object TLabeledEditProduct: TLabeledEdit
    Left = 8
    Top = 102
    Width = 300
    Height = 23
    EditLabel.Width = 49
    EditLabel.Height = 15
    EditLabel.Caption = 'Producto'
    TabOrder = 3
    Text = ''
    OnExit = TLabeledEditProductExit
    OnKeyPress = TLabeledEditProductKeyPress
  end
  object DBGridClients: TDBGrid
    Left = 8
    Top = 176
    Width = 513
    Height = 44
    DataSource = DataSourceClients
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
    Visible = False
    OnCellClick = DBGridClientsCellClick
    Columns = <
      item
        Expanded = False
        FieldName = 'ID'
        Width = 52
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NAME'
        Width = 114
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'LASTNAME'
        Width = 145
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ADDRESS'
        Width = 180
        Visible = True
      end>
  end
  object DBGridProducts: TDBGrid
    Left = 8
    Top = 236
    Width = 513
    Height = 44
    DataSource = DataSourceProducts
    TabOrder = 5
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
    Visible = False
    OnCellClick = DBGridProductsCellClick
    Columns = <
      item
        Expanded = False
        FieldName = 'ID'
        Width = 52
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NAME'
        Width = 259
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRICE'
        Width = 181
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 398
    Width = 628
    Height = 41
    Align = alBottom
    ShowCaption = False
    TabOrder = 6
    ExplicitLeft = 192
    ExplicitTop = 421
    ExplicitWidth = 185
    object SpeedButtonSave: TSpeedButton
      AlignWithMargins = True
      Left = 392
      Top = 4
      Width = 113
      Height = 33
      Align = alRight
      Caption = 'Guardar'
      OnClick = SpeedButtonSaveClick
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitHeight = 50
    end
    object SpeedButtonCancel: TSpeedButton
      AlignWithMargins = True
      Left = 511
      Top = 4
      Width = 113
      Height = 33
      Align = alRight
      Caption = 'Cancelar'
      OnClick = SpeedButtonCancelClick
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitHeight = 50
    end
  end
  object DataSourceOrdersDetails: TDataSource
    DataSet = FDQueryOrdersDetails
    Left = 456
    Top = 339
  end
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Database=C:\Data\MY_STORE.FDB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'DriverID=FB')
    Connected = True
    LoginPrompt = False
    Left = 104
    Top = 307
  end
  object FDQueryOrdersDetails: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'SELECT'
      '  od.line ORDER_LINE,'
      '  p.name PRODUCT_NAME,'
      '  od.quantity ORDER_QUANTITY'
      'FROM'
      '  orders_details od'
      'JOIN'
      '  products p ON'
      '    od.product_id = p.id'
      ';')
    Left = 456
    Top = 275
    object FDQueryOrdersDetailsORDER_LINE: TIntegerField
      FieldName = 'ORDER_LINE'
      Origin = 'LINE'
      Required = True
    end
    object FDQueryOrdersDetailsPRODUCT_NAME: TStringField
      AutoGenerateValue = arDefault
      FieldName = 'PRODUCT_NAME'
      Origin = 'NAME'
      ProviderFlags = []
      ReadOnly = True
      Size = 100
    end
    object FDQueryOrdersDetailsORDER_QUANTITY: TSingleField
      FieldName = 'ORDER_QUANTITY'
      Origin = 'QUANTITY'
      Required = True
    end
  end
  object FDQueryClients: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'SELECT * FROM clients;')
    Left = 320
    Top = 275
    object FDQueryClientsID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQueryClientsNAME: TStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Required = True
      Size = 100
    end
    object FDQueryClientsLASTNAME: TStringField
      FieldName = 'LASTNAME'
      Origin = '"LASTNAME"'
      Required = True
      Size = 100
    end
    object FDQueryClientsADDRESS: TStringField
      FieldName = 'ADDRESS'
      Origin = 'ADDRESS'
      Size = 100
    end
  end
  object DataSourceClients: TDataSource
    DataSet = FDQueryClients
    Left = 320
    Top = 339
  end
  object DataSourceProducts: TDataSource
    DataSet = FDQueryProducts
    Left = 192
    Top = 339
  end
  object FDQueryProducts: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'SELECT * FROM products;')
    Left = 192
    Top = 275
    object FDQueryProductsID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQueryProductsNAME: TStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Required = True
      Size = 100
    end
    object FDQueryProductsPRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'PRICE'
      Required = True
    end
  end
end
