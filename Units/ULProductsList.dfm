object FLProductsList: TFLProductsList
  Left = 0
  Top = 0
  Caption = 'Productos'
  ClientHeight = 462
  ClientWidth = 628
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  TextHeight = 15
  object DBGrid1: TDBGrid
    Left = 0
    Top = 0
    Width = 628
    Height = 416
    Align = alTop
    DataSource = ProductsDataSource1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -12
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'ID'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NAME'
        Width = 200
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PRICE'
        Width = 200
        Visible = True
      end>
  end
  object Panel1: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 424
    Width = 622
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    ShowCaption = False
    TabOrder = 1
    ExplicitTop = 423
    ExplicitWidth = 618
    object SpeedButton1: TSpeedButton
      AlignWithMargins = True
      Left = 268
      Top = 3
      Width = 113
      Height = 29
      Align = alRight
      Caption = 'Crear'
      OnClick = SpeedButton1Click
      ExplicitLeft = 274
      ExplicitHeight = 30
    end
    object SpeedButton2: TSpeedButton
      AlignWithMargins = True
      Left = 506
      Top = 3
      Width = 113
      Height = 29
      Align = alRight
      Caption = 'Borrar'
      OnClick = SpeedButton2Click
      ExplicitLeft = 509
      ExplicitTop = 0
    end
    object SpeedButton3: TSpeedButton
      AlignWithMargins = True
      Left = 387
      Top = 3
      Width = 113
      Height = 29
      Align = alRight
      Caption = 'Editar'
      OnClick = SpeedButton3Click
      ExplicitLeft = 424
      ExplicitTop = 11
      ExplicitHeight = 22
    end
  end
  object FDProductsQuery1: TFDQuery
    Active = True
    Connection = FDConnection1
    UpdateObject = FDUpdateSQL1
    SQL.Strings = (
      'SELECT * FROM products;')
    Left = 272
    Top = 144
    object FDProductsQuery1ID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
    end
    object FDProductsQuery1NAME: TStringField
      DisplayLabel = 'Nombre'
      FieldName = 'NAME'
      Origin = 'NAME'
      Required = True
      Size = 100
    end
    object FDProductsQuery1PRICE: TFloatField
      DisplayLabel = 'Precio'
      FieldName = 'PRICE'
      Origin = 'PRICE'
      Required = True
    end
  end
  object ProductsDataSource1: TDataSource
    DataSet = FDProductsQuery1
    Left = 384
    Top = 144
  end
  object FDConnection1: TFDConnection
    Params.Strings = (
      'Database=C:\Data\MY_STORE.FDB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'DriverID=FB')
    Connected = True
    LoginPrompt = False
    Left = 272
    Top = 88
  end
  object FDUpdateSQL1: TFDUpdateSQL
    Connection = FDConnection1
    InsertSQL.Strings = (
      'INSERT INTO PRODUCTS'
      '(NAME, PRICE)'
      'VALUES (:NEW_NAME, :NEW_PRICE)'
      'RETURNING ID, NAME, PRICE')
    ModifySQL.Strings = (
      'UPDATE PRODUCTS'
      'SET NAME = :NEW_NAME, PRICE = :NEW_PRICE'
      'WHERE ID = :OLD_ID'
      'RETURNING ID, NAME, PRICE')
    DeleteSQL.Strings = (
      'DELETE FROM PRODUCTS'
      'WHERE ID = :OLD_ID')
    FetchRowSQL.Strings = (
      'SELECT ID, NAME, PRICE'
      'FROM PRODUCTS'
      'WHERE ID = :OLD_ID')
    Left = 176
    Top = 144
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 272
    Top = 208
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 272
    Top = 264
  end
end
