unit UOrdersForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.Mask, Vcl.ExtCtrls,
  Vcl.DBCtrls, Vcl.Buttons, Data.DB, Data.FMTBcd, Data.SqlExpr, Vcl.Grids,
  Vcl.DBGrids, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error,
  FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool,
  FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB, FireDAC.Phys.FBDef,
  FireDAC.VCLUI.Wait, FireDAC.Comp.Client, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet;

type
  TFOrdersForm = class(TForm)
    SpeedButtonAddProduct: TSpeedButton;
    DBGrid1: TDBGrid;
    DataSourceOrdersDetails: TDataSource;
    FDConnection1: TFDConnection;
    FDQueryOrdersDetails: TFDQuery;
    FDQueryClients: TFDQuery;
    TLabeledEditClient: TLabeledEdit;
    TLabeledEditQuantity: TLabeledEdit;
    TLabeledEditProduct: TLabeledEdit;
    DBGridClients: TDBGrid;
    DBGridProducts: TDBGrid;
    DataSourceClients: TDataSource;
    DataSourceProducts: TDataSource;
    FDQueryProducts: TFDQuery;
    FDQueryProductsID: TIntegerField;
    FDQueryProductsNAME: TStringField;
    FDQueryProductsPRICE: TFloatField;
    FDQueryClientsID: TIntegerField;
    FDQueryClientsNAME: TStringField;
    FDQueryClientsLASTNAME: TStringField;
    FDQueryClientsADDRESS: TStringField;
    FDQueryOrdersDetailsORDER_LINE: TIntegerField;
    FDQueryOrdersDetailsPRODUCT_NAME: TStringField;
    FDQueryOrdersDetailsORDER_QUANTITY: TSingleField;
    LabelClientAddress: TLabel;
    LabelClientID: TLabel;
    LabelOrderId: TLabel;
    Panel1: TPanel;
    SpeedButtonSave: TSpeedButton;
    SpeedButtonCancel: TSpeedButton;
    procedure BeforeAddProduct();
    procedure AddProduct();
    procedure TLabeledEditClientKeyPress(Sender: TObject; var Key: Char);
    procedure TLabeledEditClientExit(Sender: TObject);
    procedure DBGridClientsCellClick(Column: TColumn);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TLabeledEditProductKeyPress(Sender: TObject; var Key: Char);
    procedure TLabeledEditProductExit(Sender: TObject);
    procedure DBGridProductsCellClick(Column: TColumn);
    procedure SpeedButtonAddProductClick(Sender: TObject);
    procedure SpeedButtonCancelClick(Sender: TObject);
    procedure SpeedButtonSaveClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FOrdersForm: TFOrdersForm;

implementation

{$R *.dfm}

uses ULOrdersList;

procedure TFOrdersForm.AddProduct;
begin
  ShowMessage('Producto A�adido');
end;

procedure TFOrdersForm.BeforeAddProduct();
begin
  if Trim(TLabeledEditQuantity.Text) = '' then
  begin
    ShowMessage('Se debe ingresar una cantidad');
    TLabeledEditQuantity.SetFocus;
  end
  else
  begin
    AddProduct()
  end;
end;

procedure TFOrdersForm.DBGridClientsCellClick(Column: TColumn);
begin
  with TLabeledEditClient do
  begin
    Enabled := False;
    Text := DBGridClients.Fields[2].AsString + ', ' + DBGridClients.Fields
      [2].AsString;
  end;

  LabelClientAddress.Caption := 'Direcci�n: ' + DBGridClients.Fields[3]
    .AsString;
  LabelClientID.Caption := 'ID: ' + DBGridClients.Fields[0].AsString;
end;

procedure TFOrdersForm.DBGridProductsCellClick(Column: TColumn);
begin
  TLabeledEditProduct.Text := DBGridProducts.Fields[1].AsString;
  TLabeledEditQuantity.SetFocus;
end;

procedure TFOrdersForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with TLabeledEditClient do
  begin
    Enabled := True;
    Text := '';
  end;

  LabelClientAddress.Caption := 'Direcci�n: N/A';
  LabelClientID.Caption := 'ID: N/A';
end;

procedure TFOrdersForm.SpeedButtonAddProductClick(Sender: TObject);
begin
  BeforeAddProduct;
end;

procedure TFOrdersForm.SpeedButtonCancelClick(Sender: TObject);
begin
  Close
end;

procedure TFOrdersForm.SpeedButtonSaveClick(Sender: TObject);
begin
  Close;
end;

procedure TFOrdersForm.TLabeledEditClientExit(Sender: TObject);
begin
  DBGridClients.Visible := False;
end;

procedure TFOrdersForm.TLabeledEditClientKeyPress(Sender: TObject;
  var Key: Char);
begin
  with FDQueryClients do
  begin
    Active := False;
    Sql.Clear;
    Sql.Text :=
      'SELECT * FROM clients WHERE LOWER(name || '' '' || lastname) LIKE LOWER(:a) OR LOWER(lastname || '' '' || name) LIKE LOWER(:a)';
    Params[0].AsString := '%' + TLabeledEditClient.Text + '%';
    Active := True;
  end;

  with DBGridClients do
  begin
    Left := TLabeledEditClient.Left;
    Top := TLabeledEditClient.Top + TLabeledEditClient.Height + 5;
    Height := 150;
    Visible := True;
  end;
end;

procedure TFOrdersForm.TLabeledEditProductExit(Sender: TObject);
begin
  DBGridProducts.Visible := False;
end;

procedure TFOrdersForm.TLabeledEditProductKeyPress(Sender: TObject;
  var Key: Char);
begin
  with FDQueryProducts do
  begin
    Active := False;
    Sql.Clear;
    Sql.Text := 'SELECT * FROM products WHERE name LIKE :a';
    Params[0].AsString := '%' + TLabeledEditProduct.Text + '%';
    Active := True;
  end;

  with DBGridProducts do
  begin
    Left := TLabeledEditProduct.Left;
    Top := TLabeledEditProduct.Top + TLabeledEditProduct.Height + 5;
    Height := 150;
    Visible := True;
  end;
end;

end.
